package com.test.movies.commons;

public class Const {
    public final static String TMDB_API_KEY = "787a5ef15cf103f7e4037a56ac233431";
    public final static String BASE_URL = "https://api.themoviedb.org/3/";
    public final static String IMAGE_URL = "https://image.tmdb.org/t/p/w500%s";

    public final static String TOP_RATED_ENDPOINT = "movie/top_rated";

    public final static String API_KEY = "api_key";
    public final static String LANGUAGE = "language";
    public final static String PAGE = "page";
}
