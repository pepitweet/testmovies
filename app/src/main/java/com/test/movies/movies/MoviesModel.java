package com.test.movies.movies;

import android.content.Context;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.test.movies.adapters.MoviesAdapter;
import com.test.movies.commons.Const;
import com.test.movies.models.TopMoviesResponse;
import com.test.movies.retrofit.RestClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesModel implements Movies.Model, MoviesAdapter.ClickListener {
    private Movies.Presenter presenter;

    private List<com.test.movies.models.Movies> list;

    public MoviesModel(Movies.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void getMovies(final Context context) {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Const.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RestClient restClient = retrofit.create(RestClient.class);

        Call<TopMoviesResponse> call = restClient.getTopRated(Const.TMDB_API_KEY, "en-US", "1");
        call.enqueue(new Callback<TopMoviesResponse>() {
            @Override
            public void onResponse(Call<TopMoviesResponse> call, Response<TopMoviesResponse> response) {
                if (response.isSuccessful()) {
                    proccessInfo(response, context);
                } else {
                    presenter.onError("Error en la consulta de información. CODE: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<TopMoviesResponse> call, Throwable t) {
                presenter.onError("Se ha producido un error al consultar la información");
            }
        });
    }

    private void proccessInfo(Response<TopMoviesResponse> response, Context context) {
        try {
            list = response.body().getResults().subList(0, 10);
            MoviesAdapter adapter = new MoviesAdapter(context, list);
            adapter.setClickListener(this);
            presenter.fillRecycler(adapter);
        } catch (Exception e) {
            presenter.onError("Error al procesar la información");
        }
    }

    @Override
    public void onItemClick(int position) {
        presenter.refresDetails(list.get(position));
    }
}
