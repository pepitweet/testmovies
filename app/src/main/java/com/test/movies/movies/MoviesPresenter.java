package com.test.movies.movies;

import android.content.Context;

import com.test.movies.adapters.MoviesAdapter;

public class MoviesPresenter implements Movies.Presenter {
    private Movies.Model model;
    private Movies.View view;

    public MoviesPresenter(Movies.View view) {
        this.view = view;
        model = new MoviesModel(this);
    }

    @Override
    public void onError(String msg) {
        view.onError(msg);
    }

    @Override
    public void refresDetails(com.test.movies.models.Movies movie) {
        view.refresDetails(movie);
    }

    @Override
    public void fillRecycler(MoviesAdapter adapter) {
        view.fillRecycler(adapter);
    }

    @Override
    public void getMovies(Context context) {
        model.getMovies(context);
    }
}
