package com.test.movies.movies;

import android.content.Context;

import com.test.movies.adapters.MoviesAdapter;

public interface Movies {
    interface View {
        void onError(String msg);
        void refresDetails(com.test.movies.models.Movies movie);
        void fillRecycler(MoviesAdapter adapter);
    }

    interface Presenter {
        void onError(String msg);
        void refresDetails(com.test.movies.models.Movies movie);
        void fillRecycler(MoviesAdapter adapter);

        void getMovies(Context context);

    }

    interface Model {
        void getMovies(Context context);
    }
}
