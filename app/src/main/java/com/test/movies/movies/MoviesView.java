package com.test.movies.movies;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;
import com.test.movies.R;
import com.test.movies.adapters.MoviesAdapter;
import com.test.movies.commons.Const;
import com.test.movies.models.TopMoviesResponse;
import com.test.movies.retrofit.RestClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesView extends AppCompatActivity implements Movies.View {
    private Movies.Presenter presenter;
    @BindView(R.id.rvMovies) RecyclerView recyclerView;
    @BindView(R.id.tvTitle) AppCompatTextView tvTitle;
    @BindView(R.id.ratingBar3) RatingBar rbRate;
    @BindView(R.id.ivBackPicture) AppCompatImageView ivIMage;
    @BindView(R.id.tvReview) AppCompatTextView tvReview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = new MoviesPresenter(this);
        presenter.getMovies(this);
    }

    @Override
    public void onError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void fillRecycler(MoviesAdapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(MoviesView.this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void refresDetails(com.test.movies.models.Movies movie) {
        tvTitle.setVisibility(View.VISIBLE);
        rbRate.setVisibility(View.VISIBLE);
        ivIMage.setVisibility(View.VISIBLE);
        tvReview.setVisibility(View.VISIBLE);

        tvTitle.setText(movie.getTitle());
        tvReview.setText(movie.getOverview());
        rbRate.setRating(movie.getVoteAverage().floatValue() / 2);

        if (movie.getBackdropPath() != null) {
            Picasso.with(this)
                    .load(String.format(Const.IMAGE_URL, movie.getBackdropPath()))
                    .into(ivIMage);
        }
    }
}
