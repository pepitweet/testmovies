package com.test.movies.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.movies.R;
import com.test.movies.commons.Const;
import com.test.movies.models.Movies;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MovieViewHolder> {
    private static ClickListener clickListener;
    private List<Movies> list;
    private Context adapterContext;

    public MoviesAdapter(Context adapterContext, List<Movies> items) {
        this.adapterContext = adapterContext;
        this.list = items;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MovieViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_movie_item, viewGroup, false));
    }

    @Override
    public int getItemCount() {
        int count = 0;
        if (list != null)
            count = list.size();
        return count;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        final Movies currentMovie = list.get(position);
        holder.tvTitle.setText(currentMovie.getTitle());
        holder.rbRate.setNumStars(5);
        holder.rbRate.setRating(currentMovie.getVoteAverage().floatValue() / 2);

        Picasso.with(adapterContext)
                .load(String.format(Const.IMAGE_URL, currentMovie.getPosterPath()))
                .into(holder.ivPoster);
    }

    public void setClickListener(ClickListener listener) {
        MoviesAdapter.clickListener = listener;
    }

    public static class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {

        @BindView(R.id.ivPoster) ImageView ivPoster;
        @BindView(R.id.tvTitle) TextView tvTitle;
        @BindView(R.id.ratingBar3) RatingBar rbRate;


        public MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d("TAG", "SIIIII");
            clickListener.onItemClick(getAdapterPosition());
        }
    }

    public interface ClickListener {
        void onItemClick(int position);
    }
}
