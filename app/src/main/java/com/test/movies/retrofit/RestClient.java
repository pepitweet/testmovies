package com.test.movies.retrofit;

import com.test.movies.commons.Const;
import com.test.movies.models.TopMoviesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RestClient {

    @GET(Const.TOP_RATED_ENDPOINT)
    Call<TopMoviesResponse> getTopRated(@Query(Const.API_KEY) String apiKey,
                     @Query(Const.LANGUAGE) String language,
                     @Query(Const.PAGE) String page);
}
